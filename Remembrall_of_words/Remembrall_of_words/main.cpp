#include <iostream>
#include <fstream>
#include <boost/filesystem/operations.hpp>
#include <windows.h>

using namespace std;
using namespace boost::filesystem;

void main() {
	HWND console = GetConsoleWindow();
	RECT r;
	::SetWindowPos(console, HWND_TOPMOST, 10, 10, 10, 5, SWP_DRAWFRAME | SWP_NOMOVE | SWP_NOSIZE | SWP_SHOWWINDOW);
	::ShowWindow(console, SW_NORMAL);
	GetWindowRect(console, &r);

//	There you can change default location of your window (X,Y,width,height)
//	MoveWindow(console, -1360, 1070, 1500, 80, TRUE);

	const char *location = "words.txt";
	const char *copylocation = "copy_words.txt";


	path _pathSource(location);
	path _pathDestination(copylocation);

	copy_file(_pathSource, _pathDestination, copy_option::overwrite_if_exists);

	time_t lastWriteTime = last_write_time(_pathSource);

	setlocale(LC_ALL, "");

	while (true) {

		if (lastWriteTime != last_write_time(_pathSource)) {
			lastWriteTime = last_write_time(_pathSource);
			copy_file(_pathSource, _pathDestination, copy_option::overwrite_if_exists);
		}

		ifstream file;
		file.open(copylocation);
		char line[100];
		int i = 0;
		while (!file.eof()) {
			file.getline(line, 100);
			cout << ++i <<". "<< line << endl;
			Sleep(3000);
		}
		file.close();
		cout << "====== END OF FILE! ======" << endl;
		Sleep(3000);
	}
}